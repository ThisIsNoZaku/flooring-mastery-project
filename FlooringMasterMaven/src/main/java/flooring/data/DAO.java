package flooring.data;

import java.io.IOException;

/**
 * Interface for Data Access Objects.
 *
 * @author apprentice
 */
interface DAO {

    /**
     * Informs the DAO to load its information and be ready to make it available
     * to the rest of the system.
     *
     * @throws IOException
     */
    public void load() throws IOException;

    /**
     * Informs the DAO to write its information to permanent storage.
     *
     * @throws IOException
     */
    public boolean save() throws IOException;
}
