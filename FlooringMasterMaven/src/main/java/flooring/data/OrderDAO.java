/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flooring.data;

import flooring.model.Order;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.OpenOption;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author apprentice
 */
public class OrderDAO implements DAO {

    private Mode mode = null;

    private int nextId = 1;
    private final Map<LocalDate, List<Order>> savedOrders = new HashMap<>();
    private String path;

    /**
     * Format used for getting dates from the user and displaying to the user.
     */
    public static final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("M-d-yyyy");
    /**
     * Format for writing to file.
     */
    private static final DateTimeFormatter fileFormat = DateTimeFormatter.ofPattern("MMddyyyy");

    /**
     * Loads the DAO configuration from the given path and sets the operation
     * mode based on the loaded configuration and the path to the directory to
     * read/write orders from/to.
     *
     * @param path the path to load
     * @throws FileNotFoundException
     */
    public void loadConfig(String path) throws FileNotFoundException {
        File config = Paths.get(path).toFile();
        Scanner scan = new Scanner(config);
        while (scan.hasNext()) {
            String[] tokens = scan.nextLine().split("=");
            switch (tokens[0]) {
                case "mode":
                    switch (tokens[1]) {
                        case "test":
                            mode = Mode.TEST;
                            break;
                        case "production":
                            mode = Mode.PRODUCTION;
                            break;
                        default:
                            throw new IllegalArgumentException("The configuration file didn't contain a valid configuration value."
                                    + "\n Must be 'test' or 'production', was " + tokens[1]);
                    }
                    break;
                case "dir":
                    this.path = tokens[1];
                    break;

            }
        }
    }

    /**
     * Load saved orders from persistent storage for use.
     *
     * @throws java.io.IOException
     */
    @Override
    public void load() throws IOException {
        if (mode == null) {
            throw new IllegalStateException("Attempted to use the dao without configuration.");
        }
        //Path to our order directory

        File inDirectory = Paths.get(path).toFile();
        if (!inDirectory.exists()) {
            inDirectory.mkdir();
        } else {
            LineSplitter splitter = new LineSplitter();
            Arrays.asList(inDirectory.listFiles()).stream().filter(file -> file.getName().matches("Orders_\\d*$")).forEachOrdered((file) -> {
                //Ignore those horrible temp files.
                int startIndex = file.getName().indexOf("_") + 1;
                LocalDate date = LocalDate.parse(file.getName().substring(startIndex), fileFormat);

                savedOrders.put(date, new ArrayList<>());
                try (Scanner scan = new Scanner(file)) {
                    while (scan.hasNext()) {
                        String in = scan.nextLine();
                        String[] tokens = splitter.split(in);

                        Order order = new Order();
                        order.setOrderNumber(Integer.parseInt(tokens[0]));
                        String name = tokens[1];
                        //Remove escape characters
                        name = name.replace("\\\\", "\\").replace("\\\"", "\"");
                        order.setCustomerName(name);
                        order.setState(tokens[2]);
                        order.setTaxRate(Double.parseDouble(tokens[3]));
                        order.setProductType(tokens[4]);
                        order.setArea(Double.parseDouble(tokens[5]));
                        order.setCostPerSquareFoot(Double.parseDouble(tokens[6]));
                        order.setLaborCostPerSquareFoot(Double.parseDouble(tokens[7]));
                        order.setMaterialCost(Double.parseDouble(tokens[8]));
                        order.setLaborCost(Double.parseDouble(tokens[9]));
                        order.setTaxAmount(Double.parseDouble(tokens[10]));
                        order.setTotal(Double.parseDouble(tokens[11]));
                        order.setDate(date);

                        if (!savedOrders.containsKey(date)) {
                            savedOrders.put(date, new ArrayList<>());
                        }
                        savedOrders.get(date).add(order);
                        if (order.getOrderNumber() >= nextId) {
                            nextId = order.getOrderNumber() + 1;
                        }
                    }
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
        }
    }

    /**
     * Saves the orders in memory to permanent storage.
     *
     * @return
     */
    @Override
    public boolean save() throws IOException {
        File outDirectory = Paths.get(path).toFile();
        if (mode == null) {
            throw new IllegalStateException("Attempted to use the dao without configuration.");
        }
        if (mode == Mode.TEST) {
            return false;
        }
        if (!outDirectory.exists()) {
            outDirectory.mkdir();
        }
        for (LocalDate date : savedOrders.keySet()) {
            String filePath = String.format("Orders_%s", fileFormat.format(date));
            File outFile = Paths.get(outDirectory.getAbsolutePath(), filePath).toFile();
            if (savedOrders.get(date).isEmpty()) {
                if (outFile.exists()) {
                    outFile.delete();
                }
            } else {
                BufferedWriter out = new BufferedWriter(new FileWriter(outFile));
                try {
                    List<String> entries = new ArrayList<>();
                    savedOrders.get(date).stream().forEachOrdered((order) -> {
//                    for (Order order : savedOrders.get(date)) {
                        String entry = String.format("%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",
                                "\"" + order.getOrderNumber() + "\"",
                                "\"" + order.getCustomerName().replace("\\", "\\\\").replace("\"", "\\\"") + "\"",
                                "\"" + order.getState() + "\"",
                                "\"" + order.getTaxRate() + "\"",
                                "\"" + order.getProductType() + "\"",
                                "\"" + order.getArea() + "\"",
                                "\"" + order.getCostPerSquareFoot() + "\"",
                                "\"" + order.getLaborCostPerSquareFoot() + "\"",
                                "\"" + order.getMaterialCost() + "\"",
                                "\"" + order.getLaborCost() + "\"",
                                "\"" + order.getTaxAmount() + "\"",
                                "\"" + order.getTotal() + "\"");
                        entries.add(entry);
                    });
                    for (String entry : entries) {
                        out.append(entry).append("\n");
                    }
                    out.flush();
                } catch (IOException ex) {

                } finally {
                    out.close();
                }
            }
        }
        return true;
    }

    /**
     * Returns a collection of all Orders for the given date, if any.
     *
     * @param date the date to find orders for
     * @return a List of the associated Orders, or an empty List if none.
     */
    public List<Order> getOrdersForDate(LocalDate date) {
        if (savedOrders.get(date) != null) {
            return new ArrayList<>(savedOrders.get(date));
        }

        return new ArrayList<>();
    }

    /**
     * Returns the order with the given order number, or null if there is none.
     *
     * @param id the order number
     * @return the order with that number
     */
    public Order getOrderByNumber(int id) {
        return getIdToOrderView().get(id);
    }

    /**
     * Adds a new Order for the given Date.
     *
     * This method takes a mutable OrderBuilder, overwrites the date and order
     * number and then stores it as an immutable Order.
     *
     * @param date
     * @param order
     */
    public void addOrder(LocalDate date, Order order) {
        order.setOrderNumber(nextId++);
        order.setDate(date);
        if (!savedOrders.containsKey(date)) {
            savedOrders.put(date, new ArrayList<>());
        }
        savedOrders.get(date).add(order);
    }

    /**
     * Removes the given order.
     *
     * @param order
     */
    public void removeOrder(Order order) {
        for (List<Order> orders : savedOrders.values()) {
            orders.remove(order);
        }
    }

    /**
     * Replaces the order with the same number as the given OrderBuilder.
     *
     * This method only replaces an order if it is already present. If no order
     * with that number is found, the underlying collection will remain
     * unchanged.
     *
     * If the new order does not have a date value set, its date will be set to
     * the date of the order it is replacing.
     *
     * @param newOrder
     * @return true if an order was replaced
     */
    public boolean updateOrder(Order newOrder) {
        for (Entry<LocalDate, List<Order>> entry : this.savedOrders.entrySet()) {
            for (Order order : entry.getValue()) {
                if (order.getOrderNumber().equals(newOrder.getOrderNumber())) {
                    entry.getValue().remove(order);
                    if (!savedOrders.containsKey(newOrder.getDate())) {
                        savedOrders.put(newOrder.getDate(), new ArrayList<>());
                    }
                    savedOrders.get(newOrder.getDate()).add(newOrder);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Returns if this contains any orders.
     *
     * @return
     */
    public boolean hasOrders() {
        return savedOrders.values().stream().mapToInt(Collection::size).sum() > 0;
    }

    private static enum Mode {

        PRODUCTION(), TEST;
    }

    /**
     * Creates a view of the orders that maps order numbers to their orders.
     *
     * @return the map view
     */
    private Map<Integer, Order> getIdToOrderView() {
        List<Order> orders = new LinkedList<>();

        for (Collection<Order> orderCollection : this.savedOrders.values()) {
            orders.addAll(orderCollection);
        }
        Map<Integer, Order> ordersById = new HashMap<>();
        for (Order order : orders) {
            ordersById.put(order.getOrderNumber(), order);
        }
        return ordersById;
    }

    /**
     * Returns all of the Dates that have orders associated with them.
     *
     * @return
     */
    public List<LocalDate> getDatesWithOrders() {
        List<LocalDate> usedDates = new ArrayList<>();
        for (LocalDate date : savedOrders.keySet()) {
            if (!savedOrders.get(date).isEmpty()) {
                usedDates.add(date);
            }
        }
        return usedDates;
    }

    /**
     * This class is incomplete, don't use it outside of the OrderDAO.
     */
    static class LineSplitter {

        public String[] split(String in) {
            String[] tokens = new String[12];
            int startQuoteIndex;
            int closeQuoteIndex;

            for (int tokenIndex = 0; tokenIndex < tokens.length; tokenIndex++) {
                startQuoteIndex = -1;
                closeQuoteIndex = -1;
                //Iterate through the string
                for (int i = 0; i < in.length(); i++) {
                    //Find a '"'
                    if (in.charAt(i) != '"') {
                        continue;
                    }
                    int backslashCount = 0;
                    if (i > 0) {
                        //If not preceded by an odd number of '\'
                        int backtrackIndex = i - 1;
                        while (backtrackIndex >= 0 && in.charAt(backtrackIndex) == '\\') {
                            backslashCount++;
                            backtrackIndex--;
                        }
                    }
                    //An even number of preceding backslashes means the found '"' is not itself escaped.
                    if (backslashCount % 2 == 0) {
                        if (startQuoteIndex == -1) {
                            startQuoteIndex = i;
                        } else if (closeQuoteIndex == -1) {
                            closeQuoteIndex = i;
                            break;
                        }
                    }
                }

                //Create token as substring from start +1 to close -1
                tokens[tokenIndex] = in.substring(startQuoteIndex + 1, closeQuoteIndex);

                //Remove the token
                in = in.substring(closeQuoteIndex + 1);
            }
            return tokens;
        }
    }
}
