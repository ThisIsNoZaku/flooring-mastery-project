/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flooring.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * Contains tax information based on state.
 */
public class TaxesDAO implements DAO {

    private final Map<String, Double> rates = new HashMap();
    private static final String filepath = "taxes.txt";

    @Override
    public void load() throws FileNotFoundException {
        File productFile = Paths.get(filepath).toFile();
        Scanner in = new Scanner(productFile);
        while (in.hasNext()) {
            String[] tokens = in.nextLine().split(",");
            rates.put(tokens[0].toUpperCase(), Double.parseDouble(tokens[1]));
        }
    }

    @Override
    public boolean save() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public Double getTaxForState(String state) {
        return rates.get(state);
    }

    public List<String> getStateNames() {

        List<String> states = new ArrayList(rates.keySet());

        return states;

    }

}
