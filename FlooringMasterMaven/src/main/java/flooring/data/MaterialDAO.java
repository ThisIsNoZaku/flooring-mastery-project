/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flooring.data;

import java.util.HashMap;
import java.util.Map;
import flooring.model.FlooringMaterial;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class MaterialDAO implements DAO {

    private final Map<String, FlooringMaterial> inventory = new HashMap<>();
    private final String filepath = "products.txt";

    @Override
    public void load() throws IOException {
        File productFile = Paths.get(filepath).toFile();
        Scanner in = new Scanner(productFile);
        while (in.hasNext()) {
            String[] tokens = in.nextLine().split(",");
            FlooringMaterial material = new FlooringMaterial(tokens[0]);
            material.setCostPerSquareFoot(Double.parseDouble(tokens[1]));
            material.setLaborCostPerSquareFoot(Double.parseDouble(tokens[2]));
            inventory.put(material.getName(), material);
        }
    }

    @Override
    public boolean save() {
        throw new UnsupportedOperationException("Material DAO should only load."); //To change body of generated methods, choose Tools | Templates.
    }

    public FlooringMaterial getMaterial(String name) {
        return inventory.get(name);
    }

    public List<String> getMaterialNames() {
        return new ArrayList<>(inventory.keySet());
    }
}
