package flooring.model;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

/**
 * Representation of an order.
 *
 * @author apprentice
 */
public class Order {

    private int orderNumber;
    private String customerName;
    private String state;
    private double taxRate;
    private String productType;
    private double area;
    private double costPerSquareFoot;
    private double laborCostPerSquareFoot;
    private double materialCost;
    private double laborCost;
    private double taxAmount;
    private double total;
    private LocalDate date;

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public void setCostPerSquareFoot(double costPerSquareFoot) {
        this.costPerSquareFoot = costPerSquareFoot;
    }

    public void setLaborCostPerSquareFoot(double laborCostPerSquareFoot) {
        this.laborCostPerSquareFoot = laborCostPerSquareFoot;
    }

    public void setMaterialCost(double materialCost) {
        this.materialCost = materialCost;
    }

    public void setLaborCost(double laborCost) {
        this.laborCost = laborCost;
    }

    public void setTaxAmount(double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * Constructor to create an uninitialized Order.
     */
    public Order() {

    }

    /**
     * Creates a new Order that is a copy of the given order.
     *
     * @param order
     */
    public Order(Order order) {
        this.orderNumber = order.orderNumber;
        this.customerName = order.customerName;
        this.state = order.state;
        this.taxRate = order.taxRate;
        this.productType = order.productType;
        this.area = order.area;
        this.costPerSquareFoot = order.costPerSquareFoot;
        this.laborCostPerSquareFoot = order.laborCostPerSquareFoot;
        this.materialCost = order.materialCost;
        this.laborCost = order.laborCost;
        this.taxAmount = order.taxAmount;
        this.total = order.total;
        this.date = order.date;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getState() {
        return state;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public String getProductType() {
        return productType;
    }

    public double getArea() {
        return area;
    }

    public double getCostPerSquareFoot() {
        return costPerSquareFoot;
    }

    public double getLaborCostPerSquareFoot() {
        return laborCostPerSquareFoot;
    }

    public double getMaterialCost() {
        return materialCost;
    }

    public double getLaborCost() {
        return laborCost;
    }

    public double getTaxAmount() {
        return taxAmount;
    }

    public double getTotal() {
        return total;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.orderNumber);
        hash = 23 * hash + Objects.hashCode(this.customerName);
        hash = 23 * hash + Objects.hashCode(this.state);
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.taxRate) ^ (Double.doubleToLongBits(this.taxRate) >>> 32));
        hash = 23 * hash + Objects.hashCode(this.productType);
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.area) ^ (Double.doubleToLongBits(this.area) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.costPerSquareFoot) ^ (Double.doubleToLongBits(this.costPerSquareFoot) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.laborCostPerSquareFoot) ^ (Double.doubleToLongBits(this.laborCostPerSquareFoot) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.materialCost) ^ (Double.doubleToLongBits(this.materialCost) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.laborCost) ^ (Double.doubleToLongBits(this.laborCost) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.taxAmount) ^ (Double.doubleToLongBits(this.taxAmount) >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.total) ^ (Double.doubleToLongBits(this.total) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Order other = (Order) obj;
        if (!Objects.equals(this.orderNumber, other.orderNumber)) {
            return false;
        }
        return true;
    }
}
