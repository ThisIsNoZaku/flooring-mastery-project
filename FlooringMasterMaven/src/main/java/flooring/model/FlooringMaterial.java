package flooring.model;

/**
 * Represents a flooring product. Combines the name, material cost per square
 * foot and labor installation cost per square foot.
 *
 * @author apprentice
 */
public class FlooringMaterial {

    private final String name;
    private double costPerSquareFoot;
    private double laborCostPerSquareFoot;

    public double getCostPerSquareFoot() {
        return costPerSquareFoot;
    }

    public void setCostPerSquareFoot(double costPerSquareFoot) {
        this.costPerSquareFoot = costPerSquareFoot;
    }

    public double getLaborCostPerSquareFoot() {
        return laborCostPerSquareFoot;
    }

    public void setLaborCostPerSquareFoot(double laborCostPerSquareFoot) {
        this.laborCostPerSquareFoot = laborCostPerSquareFoot;
    }

    public FlooringMaterial(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
