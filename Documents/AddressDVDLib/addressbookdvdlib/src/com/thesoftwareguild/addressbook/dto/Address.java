/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.addressbook.dto;

/**
 *
 * @author apprentice
 */
public class Address {
    
    private String addressNumber;
    private String street;
    private String city;
    private String State;
    private String country;
    private String zipCode;
    private String lastName;

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddressNumber() {
        return addressNumber;
    }

    public void setAddressNumber(String addressNumber) {
        this.addressNumber = addressNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String State) {
        this.State = State;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    private Integer ID;
    
//    public Address(Address address) {
//        
//        this.ID = address.ID;
//        this.lastName = address.lastName;
//        this.addressNumber = address.addressNumber;
//        this.street = address.street;
//        this.city = address.city;
//        this.State = address.State;
//        this.zipCode = address.zipCode;
//        this.country = address.country;
//        
//        
//    }
    
}
