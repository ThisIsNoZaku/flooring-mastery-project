package com.thesoftwareguild.addressbook.controller;

import com.thesoftwareguild.addressbook.con.ConsoleIO;
import com.thesoftwareguild.addressbook.dao.AddressBookDaoImpl;
import com.thesoftwareguild.addressbook.dto.Address;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class AddressBookController {

    AddressBookDaoImpl dao = new AddressBookDaoImpl();
    ConsoleIO con = new ConsoleIO();
    
    final static int MIN_OPTION_CHOICE = 1;
    final static int MAX_OPTION_CHOICE = 9;
    final static String ERROR = "NOT a valid option";

    public void run() {

        dao.load();

        boolean again = true;
        while (again) {
            menuOptions();
            int choice = con.noPromptNumber();
            choice = checkChoice(choice);

            switch (choice) {
                case 1:
                    createAddress();
                    break;
                case 2:
                    removeAddress();
                    break;
                case 3:
                    howManyAddresses();
                    break;
                case 4:
                    listAddresses();
                    break;
                case 5:
                    findAddressByLastName();
                    break;
                case 6:
                    findAddressByCity();
                    break;
                case 7:
                    findAddressByState();
                    break;
                case 8:
                    findAddressByZip();
                    break;
                case 9:
                    again = false;
                    con.print("GOODBYE!");
                    break;
                default:
                    System.out.println(ERROR);
                    break;
            }

        }
    }

    public int checkChoice(int choice) {

        while (choice < MIN_OPTION_CHOICE && choice > MAX_OPTION_CHOICE) {
            con.prompt("Please enter the number related to the options.");
        }
        return choice;
    }

    public void menuOptions() {

        con.print("================ADDRESS BOOK================");
        con.print("What would you like to do? \n"
                + "1. Create address \n"
                + "2. Remove address \n"
                + "3. View number of addresses in address book \n"
                + "4. View all addresses \n"
                + "5. Find address by last name \n"
                + "6. Find address by city \n"
                + "7. Find address by state \n"
                + "8. Find address by zipcode \n"
                + "9. Exit \n"
                + "================ADDRESS BOOK================");

    }

    public void createAddress() {
        con.print("================CREATE ADDRESS================");
        String addressNumber = con.prompt("Enter address number: ");
        String streetName = con.prompt("Enter street name: ");
        String cityName = con.prompt("Enter city name: ");
        String state = con.prompt("Enter state: ");
        String zipCode = con.prompt("Enter zipcode: ");
        String country = con.prompt("Enter country: ");

        String lastName1 = con.prompt("Enter last name: ");

        Address address = new Address();

        address.setAddressNumber(addressNumber);
        address.setStreet(streetName);
        address.setCity(cityName);
        address.setState(state);
        address.setZipCode(zipCode);
        address.setCountry(country);

        address.setLastName(lastName1);

        address = dao.create(address);

        con.print(address.getLastName() + "'s address ID is: " + address.getID());

        dao.save();
    }

    public void removeAddress() {

        Integer ID = con.promptNumber("Enter ID of address to remove");
        dao.delete(ID);

        dao.save();
    }

    public void howManyAddresses() {

        int addressCounter = 0;
        List<Address> address = dao.list();
        for (Address address1 : address) {
            addressCounter++;
        }
        if (addressCounter == 0) {
            con.print("You don't have any addresses");
        }
        if (addressCounter == 1) {
            con.print("You have one address");
        }
        if (addressCounter > 1) {
            con.print("You have " + addressCounter + " addresses");
        }

    }

    public void listAddresses() {

        List<Address> address = dao.list();
        for (Address eList : address) {
            con.print("Address ID: " + eList.getID() + "\n"
                    + "Last Name: " + eList.getLastName() + "\n"
                    + " " + eList.getAddressNumber() + "\n"
                    + " " + eList.getStreet() + "\n"
                    + " " + eList.getCity() + "\n"
                    + " " + eList.getState() + "\n"
                    + " " + eList.getZipCode() + "\n"
                    + " " + eList.getCountry());
        }

    }

    public void findAddressByLastName() {
        con.print("================FIND BY LAST NAME================");
        String lName = con.prompt("Enter last name to find");
        List<Address> address = dao.searchByLastName(lName);
        address.stream()
                .filter(a -> a.getLastName().equals(lName))
                .forEach(a -> {
                    con.print("Address ID: " + a.getID() + "\n"
                            + "Last Name: " + a.getLastName() + "\n"
                            + " " + a.getAddressNumber() + "\n"
                            + " " + a.getStreet() + "\n"
                            + " " + a.getCity() + "\n"
                            + " " + a.getState() + "\n"
                            + " " + a.getZipCode() + "\n"
                            + " " + a.getCountry());
                });
    }

    public void findAddressByCity() {
        con.print("================FIND BY CITY================");
        String city = con.prompt("Enter a city to find the address related to it: ");
        List<Address> address = dao.searchByCity(city);
        address.stream()
                .filter(a -> a.getCity().equals(city))
                .collect(Collectors.toList())
                .forEach(a -> {
                    con.print("Address ID: " + a.getID() + "\n"
                            + "Last Name: " + a.getLastName() + "\n"
                            + " " + a.getAddressNumber() + "\n"
                            + " " + a.getStreet() + "\n"
                            + " " + a.getCity() + "\n"
                            + " " + a.getState() + "\n"
                            + " " + a.getZipCode() + "\n"
                            + " " + a.getCountry());
                });

    }

    public void findAddressByState() {
        con.print("================FIND BY STATE================");
        List<Address> city = new ArrayList();
        String state = con.prompt("Enter a state to find");
        List<Address> address = dao.searchByState(state);
        address.stream()
                .filter(a -> a.getState().equals(state))
                .forEach(a -> {

                    city.add(a);
                    con.print(a.getCity());

                });
        String cityChoice = con.prompt("Enter the name of one of the cities");
        city.stream()
                .filter(c -> c.getCity().equals(cityChoice))
                .forEach(c -> {
                    con.print("Address ID: " + c.getID() + "\n"
                            + "Last Name: " + c.getLastName() + "\n"
                            + " " + c.getAddressNumber() + "\n"
                            + " " + c.getStreet() + "\n"
                            + " " + c.getCity() + "\n"
                            + " " + c.getState() + "\n"
                            + " " + c.getZipCode() + "\n"
                            + " " + c.getCountry());
                });
    }

    public void findAddressByZip() {
        con.print("================FIND BY ZIPCODE================");
        String zip = con.prompt("Enter a zipcode to find the address related to it: ");
        List<Address> address = dao.searchByZip(zip);
        address.stream()
                .filter(a -> a.getZipCode().equalsIgnoreCase(zip))
                .forEach(a -> {
                    con.print("Address ID: " + a.getID() + "\n"
                            + "Last Name: " + a.getLastName() + "\n"
                            + "Full address: " + a.getAddressNumber() + "\n"
                            + " " + a.getStreet() + "\n"
                            + " " + a.getCity() + "\n"
                            + " " + a.getState() + "\n"
                            + " " + a.getZipCode() + "\n"
                            + " " + a.getCountry());
                });

    }
}
