/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.addressbook.con;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */

public class ConsoleIO {

Scanner sc = new Scanner(System.in);
Scanner scan = new Scanner(System.in);

    public int getIntInput(String prompt) {
        System.out.println(prompt);
        int enter1 = sc.nextInt();
        return enter1;
    }
    public int minMax(String prompt, int min, int max) {
        System.out.println(prompt);
        int number = sc.nextInt();
        while (number < min || number > max) {
            System.out.println("Incorrect input! Enter any integer 1 - 10: ");
            number = sc.nextInt();
        }
        return number;
    }
    
    public void print(String prompt) {
        System.out.println(prompt);
    }
    
    public String prompt(String prompt) {
        System.out.println(prompt);
        String word = sc.nextLine();
        return word;
    }
    
    public int noPromptNumber() {
        
        int choice = scan.nextInt();
        return choice;
        
    }
    
    public int promptNumber(String prompt) {
        System.out.print(prompt);
        int choice = scan.nextInt();
        return choice;
        
    }
    
    public float coolFloat(String prompt){
        System.out.println(prompt);
        float unit = sc.nextFloat();
        return unit;
    }
    public float minMaxFloat(String prompt, int min, int max){
        System.out.println(prompt);
        float number = sc.nextFloat();
        while (number < min || number > max) {
            System.out.println("Incorrect input! Enter any float 1 - 10: ");
            number = sc.nextFloat();
        }
        return number;
    }
    public double coolDouble(String prompt){
        System.out.println(prompt);
        double unit = sc.nextDouble();
        return unit;
    }
    public double minMaxDouble(String prompt, int min, int max){
        System.out.println(prompt);
        double number = sc.nextDouble();
        while (number < min || number > max) {
            System.out.println("Incorrect input! Enter any double 1 - 10: ");
            number = sc.nextDouble();
        }
        return number;
    }
    public void printString(String prompt){
        System.out.println(prompt);
        String enter = sc.next();
        System.out.println("You typed in: "+ enter);
    }
    
}
