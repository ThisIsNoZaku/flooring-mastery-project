/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.addressbook.dao;

import com.thesoftwareguild.addressbook.dto.Address;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author apprentice
 */
public class AddressBookDaoImpl implements AddressBookDAO {

    HashMap<Integer, Address> addressBook = new HashMap<>();
    Integer lastID = 0;

    public Integer getLastID() {
        return lastID;
    }

    public void setLastID(Integer lastID) {
        this.lastID = lastID;
    }

    public List<Address> list() {
        List<Address> list = new ArrayList();

        list.addAll(addressBook.values());

        return list;

    }

    @Override
    public void delete(Integer id) {

        addressBook.remove(id);

    }

    @Override
    public List<Address> searchByLastName(String lastName) {
        List<Address> lName = new ArrayList();
        List<Address> address = list();
        for (Address eList : address) {
            if (lastName.equals(eList.getLastName())) {
                lName.add(eList);
            }
        }
        return lName;
    }

    @Override
    public List<Address> searchByCity(String city) {
        List<Address> cityName = new ArrayList();
        List<Address> Address = list();
        for (Address add : Address) {
            if (city.equals(add.getCity())) {
                cityName.add(add);
            }
        }
        return cityName;

    }

    @Override
    public Address create(Address address) {

        Integer newId = lastID++;
        address.setID(newId);

        addressBook.put(address.getID(), address);
        return address;
    }

    public void load() {

        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader("Address.txt")));

            while (sc.hasNext()) {

                String line = sc.nextLine();

                String[] properties = line.split("::");

                Address address = new Address();
                address.setID(Integer.parseInt(properties[0]));
                //makes sure if the id last added is set, make the new one and add one too it so it can keep incrimenting
                if (address.getID() > lastID) {
                    lastID = address.getID()+1;
                }
                address.setLastName(properties[1]);
                address.setAddressNumber(properties[2]);
                address.setStreet(properties[3]);
                address.setCity(properties[4]);
                address.setState(properties[5]);
                address.setZipCode(properties[6]);
                address.setCountry(properties[7]);

                addressBook.put(address.getID(), address);

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(AddressBookDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean save() {

        try {
            try (PrintWriter writer = new PrintWriter(new FileWriter("Address.txt"))) {
                for (Integer key : addressBook.keySet()) {

                    Address address = addressBook.get(key);
                    //%n means numder %s means string   they are place holders
                    writer.printf("%s::%s::%s::%s::%s::%s::%s::%s",
                            address.getID(),
                            address.getLastName(),
                            address.getAddressNumber(),
                            address.getStreet(),
                            address.getCity(),
                            address.getState(),
                            address.getZipCode(),
                            address.getCountry());

                    writer.println("");

                }

                writer.flush();
            }

        } catch (IOException ex) {
            Logger.getLogger(AddressBookDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;

    }

    @Override
    public List<Address> searchByState(String state) {
        
        List<Address> stateName = new ArrayList();
        List<Address> Address = list();
        for (Address add : Address) {
            if (state.equals(add.getState())) {
                stateName.add(add);
            }
        }
        
        return stateName;
    }

    @Override
    public List<Address> searchByZip(String zip) {
        
        List<Address> zipcode = new ArrayList();
        List<Address> Address = list();
        for (Address add : Address) {
            if (zip.equals(add.getZipCode())) {
                zipcode.add(add);
            }
        }
        
        return zipcode;
    }

    @Override
    public void update(Address address) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Address get(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
