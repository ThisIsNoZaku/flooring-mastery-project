/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.addressbook.dao;

import com.thesoftwareguild.addressbook.dto.Address;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class AddressBookDaoLambdaImpl implements AddressBookDAO {

    HashMap<Integer, Address> addressBook = new HashMap<>();
    Integer lastID = 0;

    @Override
    public List<Address> searchByLastName(String lName) {

        List<Address> list = new ArrayList();
        list.addAll(addressBook.values());
        list.stream()
                .filter(a -> a.getLastName().equals(lName))
                .forEach(a -> {
                    a.getLastName();
                    a.getAddressNumber();
                    a.getStreet();
                    a.getCity();
                    a.getState();
                    a.getZipCode();
                    a.getCountry();
                });
        return list;
    }

    @Override
    public List<Address> searchByCity(String city) {

        List<Address> list = new ArrayList();
        list.addAll(addressBook.values());
        list.stream()
                .filter(a -> a.getCity().equals(city))
                .forEach(a -> {
                    a.getLastName();
                    a.getAddressNumber();
                    a.getStreet();
                    a.getCity();
                    a.getState();
                    a.getZipCode();
                    a.getCountry();
                });
        return list;
    }

    @Override
    public List<Address> searchByState(String state) {

        List<Address> list = new ArrayList();
        list.addAll(addressBook.values());
        list.stream()
                .filter(a -> a.getCity().equals(state))
                .collect(Collectors.toList())
                .forEach(a -> {
                    a.getCity();
                });
        return list;

    }

    @Override
    public List<Address> searchByZip(String zip) {

        List<Address> list = new ArrayList();
        list.addAll(addressBook.values());
        list.stream()
                .filter(a -> a.getCity().equals(zip))
                .forEach(a -> {
                    a.getLastName();
                    a.getAddressNumber();
                    a.getStreet();
                    a.getCity();
                    a.getState();
                    a.getZipCode();
                    a.getCountry();
                });
        return list;
    }

    @Override
    public Address create(Address address) {

        Integer newId = lastID++;
        address.setID(newId);

        addressBook.put(address.getID(), address);
        return address;
    }

    @Override
    public void delete(Integer id) {

        addressBook.remove(id);

    }

    @Override
    public List<Address> list() {
        List<Address> list = new ArrayList();
        list.addAll(addressBook.values());

        list.stream()
                .forEach(a -> {
                    a.getLastName();
                    a.getAddressNumber();
                    a.getStreet();
                    a.getCity();
                    a.getState();
                    a.getZipCode();
                    a.getCountry();
                });
        return list;
    }

    @Override
    public void update(Address address) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Address get(Integer id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
