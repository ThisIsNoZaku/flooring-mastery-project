/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thesoftwareguild.addressbook.dao;

import com.thesoftwareguild.addressbook.dto.Address;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class AddressBookDaoImplTest {

    
    public AddressBookDaoImplTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        
    }

    /**
     * Test of getAllAddresses method, of class AddressBookDaoImpl.
     */
    @Test
    public void testGetAllAddresses() {
        System.out.println("getAllAddresses");
        AddressBookDaoImpl instance = new AddressBookDaoImpl();
        int expResult = 2;
        List<Address> result = instance.list();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of removeAddress method, of class AddressBookDaoImpl.
     */
    @Test
    public void testRemoveAddress() {
        System.out.println("removeAddress");
        Integer id = null;
        AddressBookDaoImpl instance = new AddressBookDaoImpl();
        instance.delete(id);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getAddressByLastName method, of class AddressBookDaoImpl.
     */
    @Test
    public void testGetAddressByLastName() {
        System.out.println("getAddressByLastName");
        String lastName = "";
        AddressBookDaoImpl instance = new AddressBookDaoImpl();
        List<Address> expResult = null;
        List<Address> result = instance.searchByLastName(lastName);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getAddressByCity method, of class AddressBookDaoImpl.
     */
    @Test
    public void testGetAddressByCity() {
        System.out.println("getAddressByCity");
        String city = "";
        AddressBookDaoImpl instance = new AddressBookDaoImpl();
        List<Address> expResult = null;
        List<Address> result = instance.searchByCity(city);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of addAddress method, of class AddressBookDaoImpl.
     */
    @Test
    public void testAddAddress() {
        System.out.println("addAddress");
        Address address = null;
        AddressBookDaoImpl instance = new AddressBookDaoImpl();
        Address expResult = null;
        Address result = instance.create(address);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of load method, of class AddressBookDaoImpl.
     */
    @Test
    public void testLoad() {
        System.out.println("load");
        AddressBookDaoImpl instance = new AddressBookDaoImpl();
        instance.load();
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of save method, of class AddressBookDaoImpl.
     */
    @Test
    public void testSave() {
        System.out.println("save");
        AddressBookDaoImpl instance = new AddressBookDaoImpl();
        boolean expResult = false;
        boolean result = instance.save();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getAddressByState method, of class AddressBookDaoImpl.
     */
    @Test
    public void testGetAddressByState() {
        System.out.println("getAddressByState");
        String state = "";
        AddressBookDaoImpl instance = new AddressBookDaoImpl();
        List<Address> expResult = null;
        List<Address> result = instance.searchByState(state);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }

    /**
     * Test of getAddressByZipcode method, of class AddressBookDaoImpl.
     */
    @Test
    public void testGetAddressByZipcode() {
        System.out.println("getAddressByZipcode");
        String zip = "";
        AddressBookDaoImpl instance = new AddressBookDaoImpl();
        List<Address> expResult = null;
        List<Address> result = instance.searchByZip(zip);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    
}
