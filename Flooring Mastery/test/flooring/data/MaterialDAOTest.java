/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flooring.data;

import flooring.data.MaterialDAO;
import flooring.model.FlooringMaterial;
import java.io.IOException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MaterialDAOTest {

    MaterialDAO materials;

    public MaterialDAOTest() {
    }

    @Before
    public void setUp() throws IOException {
        materials = new MaterialDAO();
        materials.load();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testSave() {
        materials.save();
    }

    @Test
    public void testGetMaterial() {
        assertTrue(materials.getMaterial("Laminate") != null);
    }

    @Test
    public void testGetMissingMaterial() {
        assertTrue(materials.getMaterial("Laser") == null);
    }

    @Test
    public void testGetMaterialNames() {
        assertTrue(materials.getMaterialNames().contains("Laminate"));
        assertTrue(materials.getMaterialNames().contains("Carpet"));
        assertTrue(materials.getMaterialNames().contains("Tile"));
        assertTrue(materials.getMaterialNames().contains("Wood"));
    }
}
