/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flooring.data;

import flooring.data.TaxesDAO;
import java.io.FileNotFoundException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class TaxesDAOTest {

    TaxesDAO taxes;

    public TaxesDAOTest() throws FileNotFoundException {

    }

    @Before
    public void setUp() throws FileNotFoundException {
        taxes = new TaxesDAO();
        taxes.load();
    }

    @Test
    public void testGetTaxForState() {
        assertEquals(6.25, taxes.getTaxForState("OH"), .001);
        assertEquals(6.75, taxes.getTaxForState("PA"), .001);
        assertEquals(5.75, taxes.getTaxForState("MI"), .001);
        assertEquals(6.00, taxes.getTaxForState("IN"), .001);
        assertEquals(null, taxes.getTaxForState("CA"));
    }
}
