package flooring.data;

import flooring.data.OrderDAO;
import flooring.model.Order;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class OrderDAOTest {

    OrderDAO orderDao;
    List<Order> orders;
    LocalDate date;

    @Before
    public void setUp() throws IOException {
        date = LocalDate.parse("01-01-2015", OrderDAO.dateFormat);

        orderDao = new OrderDAO();
        orderDao.loadConfig("test.config");
        orders = new ArrayList<>();

        Order order = new Order();
        order.setOrderNumber(1);
        order.setCustomerName("Christopher Walken");
        order.setState("OH");
        order.setTaxRate(6.75);
        order.setProductType("Diamond Studded");
        order.setArea(100.0);
        order.setCostPerSquareFoot(250);
        order.setLaborCostPerSquareFoot(100);
        order.setMaterialCost(order.getCostPerSquareFoot() * order.getArea());
        order.setLaborCost(order.getLaborCostPerSquareFoot() * order.getArea());
        order.setTaxAmount((order.getCostPerSquareFoot() + order.getLaborCostPerSquareFoot() * order.getTaxRate() / 100));
        order.setTotal(order.getMaterialCost() + order.getLaborCost() + order.getTaxAmount());

        orders.add(order);
        orderDao.addOrder(date, order);

        order = new Order();
        order.setCustomerName("Format Ruining Assholes/, Inc.");
        order.setOrderNumber(2);
        order.setState("OH");
        order.setTaxRate(6.75);
        order.setProductType("Laser");
        order.setArea(4000.0);
        order.setCostPerSquareFoot(150);
        order.setLaborCostPerSquareFoot(100);
        order.setMaterialCost(order.getCostPerSquareFoot() * order.getArea());
        order.setLaborCost(order.getLaborCostPerSquareFoot() * order.getArea());
        order.setTaxAmount((order.getCostPerSquareFoot() + order.getLaborCostPerSquareFoot() * order.getTaxRate() / 100));
        order.setTotal(order.getMaterialCost() + order.getLaborCost() + order.getTaxAmount());

        orders.add(order);
        orderDao.addOrder(date, order);
    }

    /**
     * Get all orders that are saved by the DAO with a given date.
     */
    @Test
    public void testGetOrdersByDate() {
        Collection<Order> savedOrders = orderDao.getOrdersForDate(date);
        //All orders in setup were added with the same date.
        for (Order order : orders) {
            assertTrue(savedOrders.contains(order));
        }
    }

    /**
     * Attempting to get orders for a date with no orders associated with it
     * returns an empty List.
     */
    @Test
    public void testGetOrdersForEmptyDate() {
        Collection<Order> savedOrders = orderDao.getOrdersForDate(LocalDate.now());
        assertTrue(savedOrders.isEmpty());
    }

    /**
     * Removing the last date associated with an order should make it no longer
     * findable.
     */
    @Test
    public void testGetOrdersForDateWithRemovedOrder() {
        Collection<Order> savedOrders = orderDao.getOrdersForDate(date);
        assertFalse(savedOrders.isEmpty());
        for (Order order : savedOrders) {
            orderDao.removeOrder(order);
        }
        savedOrders = orderDao.getOrdersForDate(date);
        assertTrue(savedOrders.isEmpty());
    }

    @Test
    public void testGetOrderById() {
        //IDs should be assigned in the order they were added.
        for (int i = 1; i <= orders.size(); i++) {
            assertTrue(orderDao.getOrderByNumber(i) != null);
        }
    }

    @Test
    public void testAddOrder() {
        orderDao.addOrder(LocalDate.now(), new Order());
        assertTrue(orderDao.getOrderByNumber(1) != null);
        assertTrue(orderDao.getOrderByNumber(2) != null);
        assertTrue(orderDao.getOrderByNumber(3) != null);
        assertTrue(orderDao.getOrderByNumber(4) == null);
    }

    @Test
    public void testRemoveOrder() {
        orderDao.removeOrder(orderDao.getOrderByNumber(1));
        assertTrue(orderDao.getOrderByNumber(1) == null);
        orderDao.removeOrder(orderDao.getOrderByNumber(2));
        assertTrue(orderDao.getOrderByNumber(2) == null);
        assertFalse(orderDao.hasOrders());
    }

    @Test
    public void testLoad() throws FileNotFoundException, IOException {
        OrderDAO newDao = new OrderDAO();

        newDao.loadConfig("test.config");
        newDao.load();
        for (Order order : orders) {
            assertTrue(newDao.getOrderByNumber(order.getOrderNumber()).equals(order));
        }
    }

    @Test
    public void testLoadConfig() throws FileNotFoundException {
        orderDao.loadConfig("test.config");
    }

    @Test(expected = FileNotFoundException.class)
    public void testLoadMissingConfig() throws FileNotFoundException {
        orderDao.loadConfig("missing.config");
    }

    @Test
    public void testGetDates() {
        assertTrue(orderDao.getDatesWithOrders().contains(date));
        assertTrue(orderDao.getDatesWithOrders().size() == 1);
    }

    @Test
    public void testReplaceOrder() {
        Order replacement = new Order();
        replacement.setOrderNumber(1);
        orderDao.updateOrder(replacement);
        assertTrue(orderDao.updateOrder(replacement));
        assertTrue(orderDao.getOrderByNumber(1).equals(replacement));
    }

    @Test
    public void testReplaceNonexistantOrder() {
        Order replacement = new Order();
        replacement.setOrderNumber(10);
        assertFalse(orderDao.updateOrder(replacement));
        assertFalse(orderDao.getOrderByNumber(1).equals(replacement));
    }

    @Test
    public void testLineSplitting() {
        String[] entries = new String[]{
            "\"1\", \",\", \"OH\", \"6.25\", \"Tile\", \"1000.0\", \"3.5\", \"4.15\", \"3500.0\", \"4150.0\", \"478.12\", \"8128.12\"",
            "\"1\", \"\\\"\", \"OH\", \"6.25\", \"Tile\", \"1000.0\", \"3.5\", \"4.15\", \"3500.0\", \"4150.0\", \"478.12\", \"8128.12\"\"",
            "\"1\", \",\\\"\", \"OH\", \"6.25\", \"Tile\", \"1000.0\", \"3.5\", \"4.15\", \"3500.0\", \"4150.0\", \"478.12\", \"8128.12\"\"",
            "\"1\", \",,,,,,,,,,,,,,,,,\", \"OH\", \"6.25\", \"Tile\", \"1000.0\", \"3.5\", \"4.15\", \"3500.0\", \"4150.0\", \"478.12\", \"8128.12\"\"",
            "\"1\", \"\\\\\\\\\", \"OH\", \"6.25\", \"Tile\", \"1000.0\", \"3.5\", \"4.15\", \"3500.0\", \"4150.0\", \"478.12\", \"8128.12\"\"",
            "\"1\", \"\\\"\\\"\\\"\\\"\\\"\", \"OH\", \"6.25\", \"Tile\", \"1000.0\", \"3.5\", \"4.15\", \"3500.0\", \"4150.0\", \"478.12\", \"8128.12\"\""};

        OrderDAO.LineSplitter splitter = new OrderDAO.LineSplitter();
        assertEquals(",", splitter.split(entries[0])[1]);
        assertEquals("\\\"", splitter.split(entries[1])[1]);
        assertEquals(",\\\"", splitter.split(entries[2])[1]);
        assertEquals(",,,,,,,,,,,,,,,,,", splitter.split(entries[3])[1]);
        assertEquals("\\\\\\\\", splitter.split(entries[4])[1]);
        assertEquals("\\\"\\\"\\\"\\\"\\\"", splitter.split(entries[5])[1]);
    }
}
