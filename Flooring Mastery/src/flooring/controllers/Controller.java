/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flooring.controllers;

import flooring.data.MaterialDAO;
import flooring.data.OrderDAO;
import java.util.Date;
import flooring.data.TaxesDAO;
import flooring.model.FlooringMaterial;
import flooring.model.Order;
import flooring.io.ConsoleIO;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class Controller {

    private final ConsoleIO io = new ConsoleIO();
    private TaxesDAO taxes = new TaxesDAO();
    private final MaterialDAO materials = new MaterialDAO();
    private final OrderDAO orders = new OrderDAO();

    //Main menu options
    private static final int DISPLAY_ORDER_OPTION = 1;
    private static final int SEARCH_BY_NUMBER_OPTION = 2;
    private static final int ADD_ORDER_OPTION = 3;
    private static final int EDIT_ORDER_OPTION = 4;
    private static final int REMOVE_ORDER_OPTION = 5;
    private static final int MAIN_EXIT = 6;
    private static final int MAIN_MENU_MIN_OPTION = 1;
    private static final int MAIN_MENU_MAX_OPTION = 6;

    //Edit order menu options
    private static final int EDIT_CUSTOMER_NAME = 1;
    private static final int EDIT_CUSTOMER_STATE = 2;
    private static final int EDIT_ORDER_MATERIAL = 3;
    private static final int EDIT_ORDER_AREA = 4;
    private static final int EDIT_ORDER_DATE = 5;
    private static final int EDIT_EXIT = 6;
    private static final int EDIT_MENU_MIN_OPTION = 1;
    private static final int EDIT_MENU_MAX_OPTION = 6;

    //Commonly displayed messages
    private static final String noOrdersMessage = "There are no orders to display";
    private static final String invalidIntegerMessage = "Your input isn't a valid integer. Please try again.";
    private static final String invalidDoubleMessage = "Your input isn't a valid number. Please try again.";
    private static final String PRESS_ENTER = "Press enter to continue...";

    public void run() throws IOException {
        //Program needs all of these files to work. Crash if they're missing.
        orders.loadConfig("production.config");
        materials.load();
        taxes.load();
        orders.load();
        boolean run = true;
        while (run) {
            //Worth main menu being it's own function?
            run = mainMenu();
        }
    }

    //Print methods 
    private void printOrder(Order order) {
        io.println(String.format("Order Number: %d", order.getOrderNumber()));
        io.println(String.format("Customer Name: %s ", order.getCustomerName()));
        io.println(String.format("Date: %s", OrderDAO.dateFormat.format(order.getDate())));
        io.println("Product Type: " + order.getProductType());
        io.println("-----------------------------------------");
        io.println("Quantity Purchased: " + order.getArea());
        io.println(String.format("Material Cost (@$%.2f per sq ft.): $%.2f", order.getCostPerSquareFoot(), order.getMaterialCost()));
        io.println(String.format("Labor Cost (@$%.2f per sq ft.): $%.2f", order.getLaborCostPerSquareFoot(), order.getLaborCost()));
        io.println(String.format("Taxes Cost (For %s @ %.2f%%) : $%.2f", order.getState(), order.getTaxRate(), order.getTaxAmount()));
        io.println("-----------------------------------------");
        io.println(String.format("Total Cost: $%.2f", order.getTotal()));
    }

    private void printOrders(List<Order> orders) {
        if (orders != null && orders.size() > 0) {
            for (Order order : orders) {
                printOrder(order);
                io.println("");
            }
        } else {
            io.println(noOrdersMessage);
        }
        io.promptForString(PRESS_ENTER);
    }

    private void printMainMenu() {
        io.println("********************************************************************");
        io.println("************ Awesome Cool Guys Flooring Company Program ************");
        io.println("********************************************************************");
        io.println("*                                                                  *");
        io.println("*1. Search Orders By Date                                          *");
        io.println("*2. Search by Order Number                                         *");
        io.println("*3. Add an Order                                                   *");
        io.println("*4. Edit an Order                                                  *");
        io.println("*5. Remove an Order                                                *");
        io.println("*6. Quit                                                           *");
        io.println("*                                                                  *");
        io.println("********************************************************************");
    }

    //Main Menu
    private boolean mainMenu() {
        while (true) {
            try {
                printMainMenu();
                int selection = io.promptForIntInRange("Choose from the menu above", MAIN_MENU_MIN_OPTION, MAIN_MENU_MAX_OPTION);
                switch (selection) {
                    case DISPLAY_ORDER_OPTION:
                        displayOrders();
                        break;
                    case SEARCH_BY_NUMBER_OPTION:
                        displayOrderByNumber();
                        break;
                    case ADD_ORDER_OPTION:
                        createOrder();
                        break;
                    case EDIT_ORDER_OPTION:
                        editOrder();
                        break;
                    case REMOVE_ORDER_OPTION:
                        removeOrder();
                        break;
                    case MAIN_EXIT:
                        return false;
                }
                break;
            } catch (NumberFormatException ex) {
                io.println(invalidIntegerMessage);
            }
        }
        return true;
    }

    private void displayOrders() {
        LocalDate date = selectDate();
        if (date != null) {
            List<Order> fetchedOrders = orders.getOrdersForDate(date);
            printOrders(fetchedOrders);
        } else {
            return;
        }
    }

    private void displayOrderByNumber() {
        while (true) {
            if (!orders.hasOrders()) {
                io.println(noOrdersMessage);
                io.promptForString(PRESS_ENTER);
                return;
            }
            Order order = selectOrderByNumber();
            if (order != null) {
                printOrder(order);
                io.promptForString(PRESS_ENTER);
                return;
            } else {
                String confirm = io.promptForString("There is no order with that number. Try again? Y/N");
                if (confirm.equalsIgnoreCase("n")) {
                    return;
                } else if (!confirm.equalsIgnoreCase("y")) {
                    io.println("That wasn't either of the two options but we'll try again anyway.");
                }
            }
        }
    }

    private void createOrder() {

        Order order = new Order();
        String name = io.promptForString("Enter the customer name: ");
        order.setCustomerName(name);
        String state = null;
        Double taxRate = null;

        while (state == null || state.length() != 2) {
            io.println("Here are your state options:" + getStateChoices());
            state = io.promptForString("Enter the two-letter customer state code: ").toUpperCase();
            if (state.length() != 2) {
                io.println("State codes must be exactly 2 characters.");
                continue;
            }
            taxRate = taxes.getTaxForState(state);
            if (taxRate == null) {
                String confirm = io.promptForString("Orders cannot be sold in " + state + ". Try again? y/n");
                if (confirm.equalsIgnoreCase("n")) {
                    return;
                } else if (!confirm.equalsIgnoreCase("y")) {
                    io.println("That wasn't an option but we'll try again.");
                }
                state = null;
            }
        }
        order.setState(state);
        order.setTaxRate(taxRate);
        io.println(String.format("The tax rate for %s is %.2f%%", order.getState(), order.getTaxRate()));

        //Display available materials.
        List<String> materials = this.materials.getMaterialNames();
        for (int i = 0; i < materials.size(); i++) {
            io.println(String.format("%d - %s", i + 1, materials.get(i)));
        }

        //Prompt user to select material
        int userSelection;
        FlooringMaterial material;
        while (true) {
            try {
                userSelection = io.promptForIntInRange("Enter number of material to select: ", 1, materials.size()) - 1;
                material = this.materials.getMaterial(materials.get(userSelection));
                break;
            } catch (NumberFormatException ex) {
                io.println(invalidIntegerMessage);
            }
        }
        order.setProductType(material.getName());
        order.setCostPerSquareFoot(material.getCostPerSquareFoot());
        order.setLaborCostPerSquareFoot(material.getLaborCostPerSquareFoot());
        //Prompt user for material area
        double area;
        while (true) {
            try {
                area = io.promptForDouble("Enter square footage of material: ");
                if (area <= 0) {
                    io.println("Quantity must be positive.");
                    io.promptForString(PRESS_ENTER);
                    continue;
                }
                break;
            } catch (NumberFormatException ex) {
                io.println(invalidDoubleMessage);
            }
        }
        order.setArea(area);
        order.setLaborCost(area * order.getLaborCostPerSquareFoot());
        order.setMaterialCost(area * order.getCostPerSquareFoot());

        calculateOrder(order);

        //Prompt user for order date
        LocalDate date;
        while (true) {
            try {
                String in = io.promptForString("Enter the date of the order in the format MM-DD-YYYY:");
                date = LocalDate.parse(in, OrderDAO.dateFormat);
                break;
            } catch (DateTimeParseException ex) {
                io.println("Your date wasn't in the correct format. Please try again.");
            }
        }
        //Make new order
        orders.addOrder(date, order);
        io.println("Order completed.");
        try {
            //Save it
            orders.save();
        } catch (IOException ex) {
            io.println("There was a problem writing to the disk. " + ex.getMessage());
        }
        io.promptForString(PRESS_ENTER);
    }

    private void editOrder() {
        if (!orders.hasOrders()) {
            io.println(noOrdersMessage);
            io.promptForString(PRESS_ENTER);
            return;
        }

        boolean edit = true;
        boolean dataChanged = false;
        Order selectedOrder = null;
        edit:
        while (edit) {
            while (selectedOrder == null) {
                selectedOrder = selectOrderByNumber();
                if (selectedOrder == null) {
                    String confirm = io.promptForString("There is no order with that number. Try again? y/n");
                    if (!confirm.equalsIgnoreCase("y")) {
                        return;
                    }
                }
                continue edit;
            }
            while (edit) {
                printOrder(selectedOrder);
                io.println("1- Customer Name");
                io.println("2- Customer State");
                io.println("3- Material");
                io.println("4- Quantity");
                io.println("5- Edit Order Date");
                io.println("6- Exit");
                try {
                    int selection = io.promptForIntInRange("Enter number of item to change", EDIT_MENU_MIN_OPTION, EDIT_MENU_MAX_OPTION);
                    io.println("");
                    switch (selection) {
                        case EDIT_CUSTOMER_NAME: {
                            String in = io.promptForString("Enter new name or blank line to leave unchanged.");
                            if (in.replace(" ", "").equals("")) {
                                break;
                            }
                            selectedOrder.setCustomerName(in);
                            dataChanged = true;
                        }
                        break;
                        case EDIT_CUSTOMER_STATE: {
                            while (true) {
                                io.println("Here are your state options:" + getStateChoices());
                                String in = io.promptForString("Enter new state or blank line to leave unchanged.");
                                if (in.replace(" ", "").equals("")) {
                                    break;
                                }
                                if (taxes.getTaxForState(in) == null) {
                                    io.println("Orders cannot be sold in " + in + ".");
                                    io.promptForString(PRESS_ENTER);
                                    continue;
                                }

                                selectedOrder.setState(in);
                                selectedOrder.setTaxRate(taxes.getTaxForState(in));
                                calculateOrder(selectedOrder);
                                dataChanged = true;
                                break;
                            }
                        }
                        break;
                        case EDIT_ORDER_MATERIAL: {
                            List<String> materialNames = this.materials.getMaterialNames();
                            for (int i = 0; i <= materialNames.size(); i++) {
                                if (i < materialNames.size()) {
                                    io.println(String.format("%d- %s", i + 1, materialNames.get(i)));
                                } else {
                                    io.println((i + 1) + "- Exit");
                                }
                            }
                            selection = io.promptForIntInRange("Select Material: ", 1, materialNames.size() + 1) - 1;
                            if (selection == materialNames.size()) {
                                continue;
                            }
                            FlooringMaterial selectedMaterial = this.materials.getMaterial(materialNames.get(selection));

                            selectedOrder.setProductType(selectedMaterial.getName());
                            selectedOrder.setCostPerSquareFoot(selectedMaterial.getCostPerSquareFoot());
                            selectedOrder.setLaborCostPerSquareFoot(selectedMaterial.getLaborCostPerSquareFoot());
                            calculateOrder(selectedOrder);
                            dataChanged = true;
                        }
                        break;
                        case EDIT_ORDER_AREA: {
                            boolean areaGood = false;
                            while (!areaGood) {
                                try {
                                    double area = io.promptForDouble("Enter new square footage of material.");
                                    if (area <= 0) {
                                        io.println("Quantity must be positive.");
                                        io.promptForString(PRESS_ENTER);
                                        continue;
                                    }
                                    selectedOrder.setArea(area);
                                    calculateOrder(selectedOrder);
                                    areaGood = true;
                                    dataChanged = true;
                                } catch (NumberFormatException ex) {
                                    io.println(invalidDoubleMessage);
                                }
                            }
                        }
                        break;
                        case EDIT_ORDER_DATE:
                            String in = io.promptForString("Enter a new date MM-DD-YYYY: ");
                            LocalDate newDate = null;
                            boolean goodDate = false;

                            while (!goodDate) {
                                try {
                                    newDate = LocalDate.parse(in, OrderDAO.dateFormat);
                                    goodDate = true;
                                } catch (DateTimeParseException ex) {
                                    in = io.promptForString("Not a valid date. Re-enter date MM-DD-YYYY.");
                                }
                            }

                            selectedOrder.setDate(newDate);
                            dataChanged = true;
                            break;
                        case EDIT_EXIT:
                            edit = false;
                            break;

                    }
                } catch (NumberFormatException ex) {
                    io.println(invalidIntegerMessage);
                }
            }
            if (dataChanged) {
                this.orders.updateOrder(selectedOrder);
                try {
                    this.orders.save();
                } catch (IOException ex) {
                    io.println("There was an error writing to the disk. " + ex.getMessage());
                }
            }
        }

    }

    private void removeOrder() {
        if (!orders.hasOrders()) {
            io.println(noOrdersMessage);
            return;
        }
        while (true) {
            Order order = null;
            while (order == null) {
                order = selectOrderByNumber();
                if (order == null) {
                    String confirm = io.promptForString("There is no order with that number. Try again? y/n");
                    if (confirm.equalsIgnoreCase("n")) {
                        return;
                    }
                    if (!confirm.equalsIgnoreCase("y")) {
                        io.println("That wasn't one of the options but let's try again.");
                    }
                }
            }

            printOrder(order);
            io.println("");

            String decision = null;
            while (true) {
                decision = io.promptForString("Are you sure you want to delete? Y/N: ");
                if (decision.equalsIgnoreCase("n")) {
                    return;
                } else if (!decision.equalsIgnoreCase("y")) {
                    io.println("Enter only a Y or an N please.");
                } else {
                    break;
                }
            }
            if (decision.equalsIgnoreCase("y")) {
                try {
                    io.println("Order number: " + order.getOrderNumber() + " was deleted");
                    orders.removeOrder(order);
                    orders.save();
                    break;
                } catch (IOException ex) {
                    io.println("There was an error writing to the disk.");
                }

            } else {
                io.println("No order was deleted");
            }
        }

        io.promptForString(PRESS_ENTER);
    }

    //Utility Methods
    private Order selectOrderByNumber() {
        int selection = -1;
        while (selection < 0) {
            try {
                String in = io.promptForString("Enter the order number: ");
                selection = Integer.parseInt(in);
            } catch (NumberFormatException ex) {
                io.println(invalidIntegerMessage);
                selection = -1;
            }
        }
        return orders.getOrderByNumber(selection);
    }

    private LocalDate selectDate() {
        while (true) {
            String dateString = io.promptForString("Enter date to get orders for in format MM-DD-YYYY or press ENTER to exit.");
            if (dateString.equals("")) {
                return null;
            }
            try {
                return LocalDate.parse(dateString, OrderDAO.dateFormat);
            } catch (DateTimeParseException ex) {
                io.println("Your input couldn't be turned into a correct date. Please try again.");
            }
        }
    }

    /**
     * Takes an order and calculates the material and labor cost, tax amount and
     * order total.
     *
     * This will override any values already assigned to those fields.
     *
     * @param order
     */
    private static void calculateOrder(Order order) {
        order.setMaterialCost(new BigDecimal(order.getCostPerSquareFoot() * order.getArea()).setScale(2, RoundingMode.HALF_EVEN).doubleValue());
        order.setLaborCost(new BigDecimal(order.getLaborCostPerSquareFoot() * order.getArea()).setScale(2, RoundingMode.HALF_EVEN).doubleValue());
        order.setTaxAmount(new BigDecimal(order.getLaborCost()).add(new BigDecimal(order.getMaterialCost())).multiply(new BigDecimal(order.getTaxRate()).divide(new BigDecimal(100))).setScale(2, RoundingMode.HALF_EVEN).doubleValue());
        order.setTotal(new BigDecimal(order.getLaborCost()).add(new BigDecimal(order.getMaterialCost())).add(new BigDecimal(order.getTaxAmount())).setScale(2, RoundingMode.HALF_EVEN).doubleValue());
    }

    /**
     * Returns a String of all the states that order can be made in.
     *
     * @return
     */
    public String getStateChoices() {

        String stateName = "";

        for (String s : taxes.getStateNames()) {
            stateName += " " + s;
        }

        return stateName;
    }

}
